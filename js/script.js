const SHAREBTN = document.querySelector(".share");
const SHARESOCIAL = document.querySelector(".share-social");
const SVG = SHAREBTN.querySelector("svg path");

SHAREBTN.addEventListener("click", () => {
    SHARESOCIAL.classList.toggle("display-social");
    SHAREBTN.classList.toggle("dark-bg");
    if(SHAREBTN.className === "share dark-bg"){
        SVG.setAttribute("fill", "#ffffff");
    }else{
        SVG.setAttribute("fill", "#6E8098");
    }
})
